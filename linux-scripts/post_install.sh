#!/bin/bash

# migrate sysadmin homedir (usermod complains if the user is logged in so do
#  this the hard way)
/bin/echo Modifying the sysadmin user
#/usr/sbin/usermod -m -d /sysadmin -a -G sudo sysadmin
/usr/bin/rsync -av /home/sysadmin /
/bin/chown -R sysadmin:sysadmin /sysadmin/
/bin/sed -i -e 's/\/home\/sysadmin/\/sysadmin/' /etc/passwd
/bin/echo

/bin/echo Post-installation is complete.  Please reboot and deploy ansible playbook on this host.
