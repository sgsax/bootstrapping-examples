#!/bin/bash

/bin/mkdir -p /common/admin
/bin/mount -t nfs -o ro,nolock files.cis.ksu.edu:/exports/admin /common/admin
/common/admin/ubuntu/bin/bionic/pre_install.sh
/common/admin/ubuntu/bin/bionic/update.sh
/common/admin/ubuntu/bin/bionic/post_install.sh
/bin/umount /common/admin
