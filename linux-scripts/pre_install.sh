#!/bin/bash

# install default apt sources
echo Copying standard CIS apt sources...
cp -v /common/admin/cfengine/masterfiles/Linux-bionic/etc/apt/sources.list /etc/apt/sources.list
cp -v /common/admin/cfengine/masterfiles/Linux-bionic/etc/apt/sources.list.d/* /etc/apt/sources.list.d/
echo

# import 3rd party repos keys
echo Importing 3rd party repository keys...
/common/admin/ubuntu/bin/bionic/add_keys.sh
echo

# report completion
echo Pre-installation is complete.  Perform CIS base package installation now.
echo workstation:   /common/admin/ubuntu/bin/bionic/update.sh
echo server:        /common/admin/ubuntu/bin/bionic/update_server.sh
