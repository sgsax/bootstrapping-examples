#!/bin/bash

# Treat unset variables as an error
set -o nounset

PATH="/usr/bin:/usr/sbin:/bin:/sbin"

export DEBIAN_FRONTEND='noninteractive'

# Update the package lists
apt update > /dev/null

# Preapprove proprietary licenses
/usr/bin/debconf-set-selections /common/admin/ubuntu/package-lists/bionic/0*

# Reset the CIS Custom Package List
INSTLIST=$(cat /common/admin/ubuntu/package-lists/bionic/[1345]* | grep install | awk '{print $1}' | xargs)

PURGELIST=$(cat /common/admin/ubuntu/package-lists/bionic/6* | grep purge | awk '{print $1}' | xargs)

# remove unwanted packages
DEBIAN_FRONTEND='noninteractive' apt -y purge $PURGELIST | grep -v "is not installed"

# install desired packages
DEBIAN_FRONTEND='noninteractive' apt -y install $INSTLIST | grep -v "is already the newest version"

# perform updates
DEBIAN_FRONTEND='noninteractive' apt -y full-upgrade
