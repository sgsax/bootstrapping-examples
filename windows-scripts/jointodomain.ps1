﻿#declaring domain variables
$domain = "win2.cs.ksu.edu"
$password = "passwordhashhere" | ConvertTo-SecureString -asPlainText -Force
$username = "$domain\imaging_svc" 
$credential = New-Object System.Management.Automation.PSCredential($username,$password)

Write-Host "*============================*"
Write-Host "*    Change Computer Name    *"
Write-Host "*============================*"
#Changing the Computers Name
$NewCompName = Read-Host "New Computer Name: "
Rename-Computer $NewCompName

#Attempting to rmove from AD
write-host "checking if name is in AD"
Invoke-Command -ComputerName ad.win2.cs.ksu.edu -Credential $credential -ScriptBlock{param($CompName) Remove-ADCOmputer -Identity $CompName} -ArgumentList $NewCompName
Start-Sleep -s 5
#Adding to the Domain
write-host "Adding to the Domain..."
$ou = 'OU=ToBeMoved,DC=win2,DC=cs,DC=ksu,DC=edu'
Add-Computer -Force -DomainName $domain -OUPath $ou -Credential $credential -ErrorAction inquire -Options AccountCreate,JoinWithNewName  -Restart
