@echo off
REM Starts the time service, and adds ntp.ksu.edu to the list

net start w32time
start /wait %SystemRoot%\system32\w32tm /config /update /manualpeerlist:ntp.ksu.edu /syncfromflags:manual